### Run binary
`$ ./server 'path/to/input.json'`

##### Env params:

- APP_PORT - port for web server to listen on (default is 80), 

    example: `$ APP_PORT=8080 ./server 'path/to/input.json'`
    
- RUST_LOG - log level, valid values are {error,warn,info,debug}, (default is info),
    
    for best performance it is recommended to set log level to `error`,
    
    log output is process stderr,

    example: `$ RUST_LOG=error ./server 'path/to/input.json'`


### Run dev server

`$ cargo run 'path/to/input.json'`

### Run tests

`$ cargo test`

### Build for release

`$ RUSTFLAGS='-C target-feature=+crt-static' cargo build --release --target x86_64-unknown-linux-gnu`

Binary will be saved at `target/x86_64-unknown-linux-gnu/release/server`
