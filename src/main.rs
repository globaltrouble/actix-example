// crate modules
#[macro_use]
mod app;
mod storage;
#[cfg(test)]
mod tests;
mod view;

// std lib imports
use std::env;
use std::sync::atomic::AtomicU64;
use std::sync::Arc;

// third party imports
use actix_web::{middleware::Logger, App, HttpServer};
use env_logger::{Builder as LogBuilder, Env as LogEnv};

// project imports
use storage::Storage;
use view::RequestCounter;

// The task itself doesn't require any async communications
// and all endpoints can serve as sync functions,
// because all views are CPU bound only, without any IO.
// But due to potential scalability in case of adding database IO,
// all endpoints are async.
// Now input is json file and all data is stored
// and processed in memory only, but to make logic more
// flexible to changes and optimize performance (on average on random queries)
// database must be used to store and process data.
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    LogBuilder::from_env(LogEnv::default().default_filter_or("info")).init();

    // port is configurable, because default port 80 require some privileges to run.
    let port = env::var("APP_PORT").unwrap_or(String::from("80"));
    let port = port
        .parse::<u16>()
        .expect(format!("Wrong port: `{}`", port).as_str());

    let storage = Arc::new(Storage::from_argv().expect("Can't load storage"));
    let counter = Arc::new(RequestCounter {
        count: AtomicU64::new(0),
        duration: AtomicU64::new(0),
    });

    HttpServer::new(move || {
        init_app!(
            App::new().wrap(Logger::default()),
            storage.clone(),
            counter.clone()
        )
    })
    .bind(("127.0.0.1", port))?
    .run()
    .await
}
