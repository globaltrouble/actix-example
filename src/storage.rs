use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use std::{env, io};

use log;
use serde::Deserialize;

pub struct Storage {
    pub vm_to_id: HashMap<String, u32>,
    pub id_to_vm: Vec<String>,
    pub tag_to_id: HashMap<String, u32>,
    pub tags_for_vm: Vec<HashSet<u32>>,
    pub src_for_dst: Vec<HashSet<u32>>,
    pub vms_for_tag: Vec<HashSet<u32>>,
}

impl Storage {
    pub fn new(input: &Input) -> Self {
        let mut s = Storage {
            vm_to_id: HashMap::new(),
            id_to_vm: Vec::new(),
            tag_to_id: HashMap::new(),
            tags_for_vm: Vec::new(),
            src_for_dst: Vec::new(),
            vms_for_tag: Vec::new(),
        };

        for vm in input.vms.iter() {
            s.store_vm(&vm.vm_id);

            for tag in vm.tags.iter() {
                s.store_tag(&tag);
                s.add_tag_to_vm(&vm.vm_id, &tag);
            }
        }

        for rule in input.fw_rules.iter() {
            s.store_rule(&rule.source_tag, &rule.dest_tag);
        }

        return s;
    }

    pub fn from_argv() -> io::Result<Self> {
        let args: Vec<String> = env::args().collect();
        if args.len() != 2 {
            log::error!(
                "Wrong number of arguments {}, usage `./server path/to/input/file.json`",
                args.len()
            );
            std::process::exit(1);
        }

        let input_path = Path::new(args[1].trim());
        return Storage::from_file(input_path);
    }

    pub fn from_file(path: &Path) -> io::Result<Self> {
        log::info!("Loading input: `{}`", path.to_str().unwrap());
        let file = File::open(path.to_str().unwrap())?;
        let reader = BufReader::new(file);
        let input = serde_json::from_reader(reader)?;
        let storage = Storage::new(&input);
        log::info!(
            "Storage vm processed={}, vm loaded={},  fw_rules: processed={}",
            input.vms.len(),
            storage.vm_to_id.len(),
            input.fw_rules.len(),
        );
        return Ok(storage);
    }

    fn store_vm(&mut self, name: &String) {
        if self.vm_to_id.contains_key(name) {
            return;
        }
        let next_id = self.id_to_vm.len() as u32;
        self.vm_to_id.insert(name.clone(), next_id);
        self.id_to_vm.push(name.clone());
    }

    fn store_tag(&mut self, tag: &String) {
        if self.tag_to_id.contains_key(tag) {
            return;
        }
        let next_id = self.tag_to_id.len() as u32;
        self.tag_to_id.insert(tag.clone(), next_id);
    }

    fn add_tag_to_vm(&mut self, vm: &String, tag: &String) {
        self.store_vm(vm);
        self.store_tag(tag);

        let vm_id = *self.vm_to_id.get(vm).unwrap() as usize;
        let tag_id = *self.tag_to_id.get(tag).unwrap() as usize;

        if self.tags_for_vm.len() <= vm_id {
            self.tags_for_vm.resize_with(vm_id + 1, || HashSet::new());
        }
        self.tags_for_vm[vm_id].insert(tag_id as u32);

        if self.vms_for_tag.len() <= tag_id {
            self.vms_for_tag.resize_with(tag_id + 1, || HashSet::new());
        }
        self.vms_for_tag[tag_id].insert(vm_id as u32);
    }

    fn store_rule(&mut self, src_tag: &String, dst_tag: &String) {
        self.store_tag(src_tag);
        self.store_tag(dst_tag);

        let src_id = *self.tag_to_id.get(src_tag).unwrap();
        let dst_id = *self.tag_to_id.get(dst_tag).unwrap() as usize;

        if dst_id >= self.src_for_dst.len() {
            self.src_for_dst.resize_with(dst_id + 1, || HashSet::new());
        }
        self.src_for_dst[dst_id].insert(src_id);
    }
}

#[derive(Deserialize)]
pub struct Input {
    pub vms: Vec<Vm>,
    pub fw_rules: Vec<Rule>,
}

#[derive(Deserialize)]
pub struct Vm {
    pub vm_id: String,
    #[allow(dead_code)]
    pub name: String,
    pub tags: Vec<String>,
}

#[derive(Deserialize)]
pub struct Rule {
    #[allow(dead_code)]
    pub fw_id: String,
    pub source_tag: String,
    pub dest_tag: String,
}
