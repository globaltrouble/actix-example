// Tests are moved to a separate module, because endpoints depends
// on global context (RequestCounter), middleware and other endpoints.
// So test it with context of web app instead of single view unit testing.

use actix_web::http;
use actix_web::test::{self, TestRequest};
use more_asserts::assert_gt;

use crate::storage::Input;
use crate::view::ApiStats;

macro_rules! create_app {
    ($raw_input: expr) => {{
        use actix_web::App;
        use std::sync::atomic::AtomicU64;
        use std::sync::Arc;

        use crate::storage::Storage;
        use crate::view::RequestCounter;

        let input: Input = serde_json::from_str($raw_input).unwrap();
        let storage = Arc::new(Storage::new(&input));
        let counter = Arc::new(RequestCounter {
            count: AtomicU64::new(0),
            duration: AtomicU64::new(0),
        });
        test::init_service(init_app!(App::new(), storage.clone(), counter.clone())).await
    }};
}

const INPUT_RELATIONS: &str = r#"{
    "vms": [
        {"vm_id": "vm-a211de", "name": "jira_server", "tags": ["ci", "dev"]},
        {"vm_id": "vm-c7bac01a07", "name": "bastion", "tags": ["ssh", "dev", "same-tag"]},
        {"vm_id": "vm-t1000", "name": "modem", "tags": ["apache", "same-tag"]},
        {"vm_id": "ar2d2", "name": "modem", "tags": []}
    ],
    "fw_rules": [
        {"fw_id": "fw-82af742", "source_tag": "ssh", "dest_tag": "dev"},
        {"fw_id": "cycle", "source_tag": "same-tag", "dest_tag": "same-tag"},
        {"fw_id": "deploy", "source_tag": "ci", "dest_tag": "apache"}
    ]
}"#;

// Skip format validation, just test logic, because format is the same as struct definition
// it's overkill to test serializer framework logic for such trivial task.

#[actix_rt::test]
async fn test_get_stats() {
    let mut app = create_app!(INPUT_RELATIONS);

    // current request must not be counted because during response it is still in progress.
    let resp = TestRequest::get()
        .uri("/api/v1/stats")
        .send_request(&mut app)
        .await;

    assert!(resp.status().is_success());
    let stats: ApiStats = test::read_body_json(resp).await;
    assert_eq!(stats.vm_count, 4);
    assert_eq!(stats.request_count, 0);
    assert_eq!(stats.average_request_time, 0f64);

    // request to other endpoints will be tracked
    let resp = TestRequest::get()
        .uri("/api/v1/attack?vm_id=42")
        .send_request(&mut app)
        .await;
    assert!(resp.status().is_success());

    // invalid requests will be tracked too
    let resp = TestRequest::get()
        .uri("/api/v1/attack")
        .send_request(&mut app)
        .await;
    assert!(resp.status().is_client_error());

    // final response must contains data for all previous requests
    let resp = TestRequest::get()
        .uri("/api/v1/stats")
        .send_request(&mut app)
        .await;

    assert!(resp.status().is_success());
    let stats: ApiStats = test::read_body_json(resp).await;
    assert_eq!(stats.vm_count, 4);
    assert_eq!(stats.request_count, 3);
    assert_gt!(stats.average_request_time, 0f64);
}

#[actix_rt::test]
async fn test_get_attack_query_string() {
    let mut app = create_app!(INPUT_RELATIONS);

    // no qs
    let resp = TestRequest::get()
        .uri("/api/v1/attack")
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);

    // qs with invalid key
    let resp = TestRequest::get()
        .uri("/api/v1/attack?vm_DD")
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);

    // qs with valid key, but without vm_id value
    let resp = TestRequest::get()
        .uri("/api/v1/attack?vm_id")
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);

    // qs with valid key and existing vm_id value
    let resp = TestRequest::get()
        .uri("/api/v1/attack?vm_id=vm-a211de")
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::OK);

    // qs with valid key and non existing vm_id is not an error
    let resp = TestRequest::get()
        .uri("/api/v1/attack?vm_id=42")
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::OK);
}

#[actix_rt::test]
async fn test_get_attack_unknown_vm() {
    let mut app = create_app!(INPUT_RELATIONS);

    // existing vm
    let resp = TestRequest::get()
        .uri("/api/v1/attack?vm_id=vm-a211de")
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::OK);

    let actual: Vec<String> = test::read_body_json(resp).await;
    assert_eq!(actual.len(), 1);
    assert_eq!(actual[0].as_str(), "vm-c7bac01a07");

    // non existing vm is not an error,
    // zero attackers for non existing vm
    let resp = TestRequest::get()
        .uri("/api/v1/attack?vm_id=42")
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::OK);
    let src_vms: Vec<String> = test::read_body_json(resp).await;
    assert_eq!(src_vms.len(), 0);
}

#[actix_rt::test]
async fn test_get_attack_same_vm() {
    let mut app = create_app!(INPUT_RELATIONS);

    // vm can't access itself
    let vm_id = String::from("vm-a211de");
    let resp = TestRequest::get()
        .uri(format!("/api/v1/attack?vm_id={}", vm_id.as_str()).as_str())
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::OK);
    let actual: Vec<String> = test::read_body_json(resp).await;
    assert!(!actual.contains(&vm_id));

    // vm can access itself, but filtered because it can't be the source of attack for itself
    let vm_id = String::from("vm-c7bac01a07");
    let resp = TestRequest::get()
        .uri(format!("/api/v1/attack?vm_id={}", vm_id.as_str()).as_str())
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::OK);
    let actual: Vec<String> = test::read_body_json(resp).await;
    assert!(!actual.contains(&vm_id));
}

#[actix_rt::test]
async fn test_get_attack_same_tag() {
    let mut app = create_app!(INPUT_RELATIONS);

    // same tag without rule
    let vm_id = String::from("vm-c7bac01a07");
    let other_id = String::from("vm-a211de");
    let resp = TestRequest::get()
        .uri(format!("/api/v1/attack?vm_id={}", vm_id.as_str()).as_str())
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::OK);
    let actual: Vec<String> = test::read_body_json(resp).await;
    assert!(!actual.contains(&other_id));

    // same tag with rule
    let other_id = String::from("vm-t1000");
    let resp = TestRequest::get()
        .uri(format!("/api/v1/attack?vm_id={}", vm_id.as_str()).as_str())
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::OK);
    let actual: Vec<String> = test::read_body_json(resp).await;
    assert!(actual.contains(&other_id));
}

#[actix_rt::test]
async fn test_get_attack_no_tags() {
    let mut app = create_app!(INPUT_RELATIONS);

    // no vm can reach vm without tags
    let without_tags = String::from("ar2d2");
    let resp = TestRequest::get()
        .uri(format!("/api/v1/attack?vm_id={}", without_tags.as_str()).as_str())
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::OK);
    let sources: Vec<String> = test::read_body_json(resp).await;
    assert_eq!(sources.len(), 0);

    // vm without tags can reach no vm
    let with_tags: Vec<String> = vec!["vm-a211de", "vm-t1000", "vm-c7bac01a07"]
        .iter()
        .map(|s| String::from(*s))
        .collect();
    for item in with_tags {
        let resp = TestRequest::get()
            .uri(format!("/api/v1/attack?vm_id={}", item.as_str()).as_str())
            .send_request(&mut app)
            .await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        let sources: Vec<String> = test::read_body_json(resp).await;
        assert!(!sources.contains(&without_tags));
    }
}

#[actix_rt::test]
async fn test_get_attack_not_transitive() {
    let mut app = create_app!(INPUT_RELATIONS);

    // a -> b && b -> c && c -> a && !(a -> c)
    let a = String::from("vm-t1000");
    let b = String::from("vm-c7bac01a07");
    let c = String::from("vm-a211de");

    // a -> b
    let resp = TestRequest::get()
        .uri(format!("/api/v1/attack?vm_id={}", b.as_str()).as_str())
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::OK);
    let sources: Vec<String> = test::read_body_json(resp).await;
    assert!(sources.contains(&a));

    // b -> c
    let resp = TestRequest::get()
        .uri(format!("/api/v1/attack?vm_id={}", c.as_str()).as_str())
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::OK);
    let sources: Vec<String> = test::read_body_json(resp).await;
    assert!(sources.contains(&b));

    // c -> a
    let resp = TestRequest::get()
        .uri(format!("/api/v1/attack?vm_id={}", a.as_str()).as_str())
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::OK);
    let sources: Vec<String> = test::read_body_json(resp).await;
    assert!(sources.contains(&c));

    // !(a -> c)
    let resp = TestRequest::get()
        .uri(format!("/api/v1/attack?vm_id={}", c.as_str()).as_str())
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::OK);
    let sources: Vec<String> = test::read_body_json(resp).await;
    assert!(!sources.contains(&c));
}

#[actix_rt::test]
async fn test_wrong_endpoint() {
    let mut app = create_app!(INPUT_RELATIONS);

    // valid endpoint with valid method
    let resp = TestRequest::get()
        .uri("/api/v1/stats")
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::OK);

    // another valid endpoint with valid method
    let resp = TestRequest::get()
        .uri("/api/v1/attack?vm_id=42")
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::OK);

    // valid endpoint, invalid method
    let resp = TestRequest::post()
        .uri("/api/v1/stats")
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::NOT_FOUND);

    // another valid endpoint with invalid method
    let resp = TestRequest::post()
        .uri("/api/v1/attack?vm_id=42")
        .send_request(&mut app)
        .await;
    assert_eq!(resp.status(), http::StatusCode::NOT_FOUND);

    // root endpoint is error
    let resp = TestRequest::get().uri("/").send_request(&mut app).await;
    assert_eq!(resp.status(), http::StatusCode::NOT_FOUND);
}
