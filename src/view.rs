use std::collections::HashSet;
use std::sync::atomic::AtomicU64;
use std::sync::{atomic, Arc};

use actix_web::{web, HttpRequest, HttpResponse, Responder};
use qstring::QString;
use serde::{Deserialize, Serialize};
use serde_json::json;

use crate::storage::Storage;

// Views are mixed with helpers and serializers, because it is overkill,
// to place single function/struct to a separate module for such trivial task.

pub async fn get_attack(req: HttpRequest, context: web::Data<Arc<Storage>>) -> impl Responder {
    let qs = QString::from(req.query_string());
    let vm = qs.get("vm_id");
    if vm.is_none() || vm.as_ref().unwrap().is_empty() {
        log::warn!("Unknown vm_id: `{}`", vm.unwrap_or(""));
        let err = AttackError {
            reason: String::from("vm_id is mandatory query string parameter"),
        };
        return HttpResponse::BadRequest().json(err);
    }

    let vm = vm.unwrap();
    let sources = get_source_vms_for_vm(&context.get_ref(), vm.to_string());
    // Didn't create separate serializer for sources,
    // because it is primitive data type (array of strings),
    // just convert it to json.
    return HttpResponse::Ok().body(json!(sources).to_string());
}

fn get_source_vms_for_vm(storage: &Storage, vm: String) -> Vec<String> {
    let vm_id = storage.vm_to_id.get(&vm);
    if vm_id.is_none() {
        // Probably unknown vm must be treated as error but it was not mentioned in assignment
        return Vec::new();
    }

    let vm_id = *vm_id.unwrap() as usize;
    if vm_id >= storage.tags_for_vm.len() {
        return Vec::new();
    }

    let dst_tags = &storage.tags_for_vm[vm_id];
    let mut src_tags = HashSet::new();
    for tag in dst_tags.iter() {
        let tag = *tag as usize;
        if tag >= storage.src_for_dst.len() {
            continue;
        }
        for src in storage.src_for_dst[tag].iter() {
            src_tags.insert(*src);
        }
    }

    let mut sources = HashSet::new();
    let mut result = Vec::new();
    for tag in src_tags {
        let tag = tag as usize;
        if tag >= storage.vms_for_tag.len() {
            continue;
        }
        for src_vm in storage.vms_for_tag[tag].iter() {
            // It wasn't mentioned in requirements, but I think, that
            // current vm must be filtered in result,
            // because it can't be the source of attack to itself.
            // TODO: Discuss!
            if sources.insert(*src_vm) && *src_vm != vm_id as u32 {
                result.push(storage.id_to_vm[*src_vm as usize].clone())
            }
        }
    }

    return result;
}

pub async fn get_stats(
    counter: web::Data<Arc<RequestCounter>>,
    storage: web::Data<Arc<Storage>>,
) -> impl Responder {
    let count = counter.count.load(atomic::Ordering::Relaxed);
    let duration = counter.duration.load(atomic::Ordering::Relaxed);
    let avg_dur = if count == 0 {
        0_f64
    } else {
        // mul `count` instead of div `duration` to avoid lose of floating point precision
        duration as f64 / (count as f64 * 1_000_000_f64)
    };

    let stats = ApiStats {
        vm_count: storage.vm_to_id.len() as u64,
        request_count: count,
        average_request_time: avg_dur,
    };
    return HttpResponse::Ok().json(stats);
}

#[derive(Serialize, Deserialize)]
pub struct AttackError {
    pub reason: String,
}

#[derive(Serialize, Deserialize)]
pub struct ApiStats {
    pub vm_count: u64,
    pub request_count: u64,
    pub average_request_time: f64,
}

pub struct RequestCounter {
    pub count: AtomicU64,
    pub duration: AtomicU64,
}
