// Wrapped to macro to reuse code in tests,
// because tests must use same setup process.
macro_rules! init_app {
    ($app: expr, $storage: expr, $counter: expr) => {{
        use std::sync::atomic::Ordering::Relaxed;
        use std::time::Instant;

        use actix_web::dev::Service;
        use actix_web::web;
        use futures::FutureExt;

        use crate::view::{get_attack, get_stats};

        const BASE_URL: &str = "/api/v1/";

        // each worker must have clone of global counter, no one must own/move it.
        let counter = $counter.clone();
        $app.data($storage.clone())
            .data(counter.clone())
            .service(
                web::scope(BASE_URL)
                    .route("attack", web::get().to(get_attack))
                    .route("stats", web::get().to(get_stats)),
            )
            .wrap_fn(move |req, srv| {
                // dirty hack to capture counter from worker without moving it.
                let counter = counter.clone();
                let start = Instant::now();
                srv.call(req).map(move |resp| {
                    counter.count.fetch_add(1u64, Relaxed);
                    let elapsed = start.elapsed().as_micros() as u64;
                    counter.duration.fetch_add(elapsed, Relaxed);
                    resp
                })
            })
    }};
}
